"use strict";
process.env.NTBA_FIX_319 = 1;
process.env.NTBA_FIX_350 =1;

const fs = require('fs');
const uuid = require('uuid');
const dialogflow = require('@google-cloud/dialogflow');
const { struct } = require('pb-util'); 
const readline = require('readline');
const {google} = require('googleapis');
const textToSpeech = require('@google-cloud/text-to-speech');
const speech = require('@google-cloud/speech');
const TelegramBot = require("node-telegram-bot-api");
const util = require('util'); 
const path = require('path'); 

const SCOPES = ['https://www.googleapis.com/auth/calendar.events'];
const telegramToken = "1004152302:AAGb0fD6-Ml71txgA6dPed0f5SZr1fEi6m8";
const calendarToken = 'v93hi7gn9btsg2sdk3ivjdgdl8@group.calendar.google.com';
const projectId = 'feriebot3-trdymp';

const utilityPath = path.join (path.dirname(process.execPath), 'utility');
//const dataPath = path.join (utilityPath, 'data.json');
const dataPath = 'data.json';
const audioOutputPath = path.join (utilityPath, 'audioOutput');
const audioInputPath = path.join (utilityPath, 'audioInput');
const credentialsPath =  path.join(__dirname, 'stuff', 'credentials.json');
const TOKEN_PATH =  path.join(__dirname, 'stuff', 'token.json');
const keyFilename =  path.join(__dirname, 'stuff', 'FerieBot3-91b7e3ad2ac7.json');

const bot = new TelegramBot(telegramToken, {polling: true});
const ttsClient = new textToSpeech.TextToSpeechClient({projectId, keyFilename});
const sttClient = new speech.SpeechClient({projectId, keyFilename});
const sessionClient = new dialogflow.SessionsClient({projectId, keyFilename});
const contextsClient = new dialogflow.ContextsClient({projectId, keyFilename});

const MORNING_START = '09:00:00';
const MORNING_END = '13:00:00';
const AFTERNOON_START = '14:00:00';
const AFTERNOON_END = '18:00:00';
const TIMEZONE = '+02:00';
const MAX_FALLBACKS = 3;
const MILLISEC_DAY = 86400000;
const breaktime = 1;
const approveOpts =  {
  reply_markup: {
      inline_keyboard: [
        [{ text: 'Approva', callback_data: 'yAdmin' },
        { text: 'Rifiuta', callback_data: 'nAdmin' }],
      ]
  }
};
const MAX_RETRIES = 5000;
let googleRetry = 0;
let data = '';

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

function jsonWriter(data){
  fs.writeFile(dataPath, JSON.stringify(data, null, 2), (err) => {
    if (err) console.log('Error writing file:', err)
  })
}

/**
 * FUNZIONI PER LAVORARE SULLE DATE
 */
  /** Aggiunge alla data tanti giorni quanti indicati dalla variabile days */
  Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
  }

  /** Ritorna un array di data comprese fra startDate e stopDate */
  function getDates(startDate, stopDate) {
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push(new Date (currentDate));
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
  }

  /** Dato un numero restituito dal getDay() di una Date, restituisce la stringa corrisponde a quel giorno della settimana */
  function getWeekDay(number){
    switch (number) {
      case 0: return "Domenica";
      case 1: return "Lunedì";
      case 2: return "Martedì";
      case 3: return "Mercoledì";
      case 4: return "Giovedì";
      case 5: return "Venerdì";
      case 6: return "Sabato";
    }
  }

  /*controlla se la data è in un giorno festivo*/
  function isFestivo(date){
    let dd = date.getDate(); 
    let mm = date.getMonth() + 1; 
    if (dd < 10) { 
        dd = '0' + dd; 
    } 
    if (mm < 10) { 
        mm = '0' + mm; 
    } 
    const dateStr = dd + '/' + mm; 
    const festivi = ['25/12', '25/04', '01/01', '31/12', '01/05', '02/06', '15/08', '01/11', '24/06'];
    if (date.getDay() == 0 || date.getDay() == 6 || festivi.filter((f) => {return f==dateStr;}).length >= 1){
      return true;
    } else {
      return false;
    }
  }

  /** Data una data, restituisce un stringa nel formato ISOString con la TimeZone corretta */
  function dateToString(date){
    return new Date(date.getTime() - (date.getTimezoneOffset() * 60000)).toISOString();
  }

/**
 * FUNZIONI PER USARE LE STRINGHE RESTITUITE DA DIALOGFLOW O SUL FILE JSON
 */
  /** Interpreta l'orario facendolo rientrare in quello lavorativo (in questo caso dalle 8 alle 20), 
  necessario in quanto spesso DialogFlow interpreta ad esempio "alle 9" come 21:00 */
  function getRightHour(datetime){
    let hours = parseInt(datetime.substring(11,13));
    if (hours > 20){
      hours -= 12;
    }
    if (hours < 8){
      hours += 12;
    }
    hours = ("00" + hours).substr(-2,2);
    return datetime.substring(0,11) + hours + datetime.substring(13);
  }

  /** Interpreta gli orari di mattina e pomeriggio forniti da Dialogflow mappandoli su quelli canonici 9-13 / 14-18
   * ed usa getRightHour per ottenere gli orari corretti*/
  function getRightTime(periodo){
    const newPeriodo = {};
    if (periodo.startTime.substring(11,16) == "05:00" && periodo.endTime.substring(11, 16) == "11:59"){
      newPeriodo.startDateTime = periodo.startTime.substring(0,10) + 'T'+MORNING_START+TIMEZONE;
      newPeriodo.endDateTime = periodo.endTime.substring(0,10) + 'T'+MORNING_END+TIMEZONE;
    } else if (periodo.startTime.substring(11,16) == "12:00" && periodo.endTime.substring(11, 16) == "17:59"){
      newPeriodo.startDateTime = periodo.startTime.substring(0,10) + 'T'+AFTERNOON_START+TIMEZONE;
      newPeriodo.endDateTime = periodo.endTime.substring(0,10) + 'T'+AFTERNOON_END+TIMEZONE;
    } else {
      newPeriodo.startDateTime = getRightHour(periodo.startTime);
      newPeriodo.endDateTime = getRightHour(periodo.endTime);
    }
    if (new Date(newPeriodo.startDateTime) > new Date(newPeriodo.endDateTime)){
      let temp = newPeriodo.startDateTime;
      newPeriodo.startDateTime = newPeriodo.endDateTime;
      newPeriodo.endDateTime = temp;
    }
    return newPeriodo;
  }

  /** Da una data passata come ISOstring restituisce la data in formato dd/mm/yyyy */
  function getDate(date){
    return date.substring(8,10) + '/' + date.substring(5,7)  + '/' + date.substring(0,4);
  }

  /** Da un periodo passato come ISOstring restituisce una stringa che descrive il periodo */
  function getPeriodo(periodo, showOrario918 = true){
    let message = "";
    if (periodo.startDateTime.substring(0,10) == periodo.endDateTime.substring(0,10)){
      const day = periodo.startDateTime.substring(8,10);
      message += day == '08' || day == '01' || day == '11' ? "l'" : "il ";
      message += getDate(periodo.startDateTime); 
      if (showOrario918){
        message +=' dalle ';
        message += periodo.startDateTime.substring(11) == 0 ? periodo.startDateTime.substring(12,16) : periodo.startDateTime.substring(11,16); 
        message +=' alle ';
        message += periodo.endDateTime.substring(11,16);
      }
    } else {
      message += `dal ${getDate(periodo.startDateTime)} al ${getDate(periodo.endDateTime)}`;
    }
    if (!(periodo.startDateTime.substring(11,19) == MORNING_START && periodo.endDateTime.substring(11,19) == AFTERNOON_END) && !showOrario918){
      message +=' dalle ';
      message += periodo.startDateTime.substring(11) == 0 ? periodo.startDateTime.substring(12,16) : periodo.startDateTime.substring(11,16); 
      message +=' alle ';
      message += periodo.endDateTime.substring(11,16);
    }
    return message;
  }

  /** Restituisce il messaggio da inviare all'amministratore in base alla modalità (creazione e o cancellazione)*/
  function getMessage(toRequest, mode){
    const periodo = toRequest.date;
    let message = toRequest.user.first_name;
    if (toRequest.user.hasOwnProperty('last_name')){
      message += ' ';
      message += toRequest.user.last_name;
    }
    if (mode == "create"){
      message += periodo.startDateTime.substring(0,10) == periodo.endDateTime.substring(0,10) ? ' vorrebbe chiedere un permesso ' : ' vorrebbe chiedere delle ferie ';
    } else if (mode == "delete"){
      message += periodo.startDateTime.substring(0,10) == periodo.endDateTime.substring(0,10) ? ' ha cancellato un permesso ' : ' ha cancellato i permessi ';
    }
    message += getPeriodo(periodo);
    message += '.';
    return message;
  }

/**
 * FUNZIONI DIALOGFLOW
 */
  /** Crea un contesto per poterlo inviare in query a Dialogflow */
  async function createContext(sessionId, contextId, parameters, lifespanCount = 2) {
    const request = {
        parent: `projects/${projectId}/agent/sessions/${sessionId}`,
        context: {
            name: `projects/${projectId}/agent/sessions/${sessionId}/contexts/${contextId}`,
            parameters: struct.encode(parameters),
            lifespanCount
        }
    };

    const [context] = await contextsClient.createContext(request);
    return context;
  }

  /** Invia una query a Dialogflow */
  async function sendQuery(sessionId, query, context=null) {

    const request = {
      session: `projects/${projectId}/agent/sessions/${sessionId}`,
      queryInput: {
          text: {
              text: query,
              languageCode: 'it-IT'
          }
      },
    };

    if (context != null) {
      request.queryParams = {
          contexts: [context]
      };
    }

    return sessionClient.detectIntent(request)
  }

/**
 * FUNZIONI GOOGLE CALENDAR API
 */
  /** Autorizzazione per agire su Google Calendar */
  function authorize(credentials, callback) {
    const {client_secret, client_id, redirect_uris} = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0]);

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, (err, token) => {
      if (err) return getAccessToken(oAuth2Client, callback);
      oAuth2Client.setCredentials(JSON.parse(token));
      callback(oAuth2Client);
    });
  }

  /** Autenticazione al Google Calendar */
  function getAccessToken(oAuth2Client, callback) {
    const authUrl = oAuth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code) => {
      rl.close();
      oAuth2Client.getToken(code, (err, token) => {
        if (err) return console.error('Error retrieving access token', err);
        oAuth2Client.setCredentials(token);
        // Store the token to disk for later program executions
        fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
          if (err) return console.error(err);
          console.log('Token stored to', TOKEN_PATH);
        });
        callback(oAuth2Client);
      });
    });
  }

  /**
   * Cerca eventi nel calendario
   * @param {*} from data di inizio
   * @param {*} to data di fine
   * @param {*} callback fuzione da chiamare successivamente
   * @param {*} id se presente, cerca solo gli eventi dei un certo dipendente
   */
  function listEvents(chat_id, from, to, callback, id = null){
    fs.readFile(credentialsPath, (err, content) => {
      if (err) return console.log('Error loading client secret file:', err);
      authorize(JSON.parse(content), (auth) => {
        const calendar = google.calendar({version: 'v3', auth});
        calendar.events.list({
          calendarId: calendarToken,
          timeMin: from.toISOString(),
          timeMax: to.toISOString(),
          maxResults: 500,
          singleEvents: true,
          orderBy: 'startTime',
        }, (err, res) => {
          if (err) {
            googleRetry++;
            if (googleRetry < MAX_RETRIES){
              if (err.code == 403){
                console.log('Rate Limit Error, retry');
              } else {
                console.log('Delete event error: ' + err + ', retry');
              }
              setTimeout(listEvents, 2000, chat_id, from, to, callback);
            } else {
              if (googleRetry == MAX_RETRIES){
                const answer = "E' stato raggiunto il numero massimo di tentativi e non è stato possibile portare a termine l'operazione, riprova in un altro momento.";
                data.tts[chat_id] ? sendAudio(answer, "richiesta_già_esaudita.mp3", chat_id, true) : bot.sendMessage(chat_id, answer);
              }
            } 
            return;
          }
          let events = res.data.items;
          if (id != null){
            events = events.filter(e => {return e.location == id});
          }
          if (events.length) {
            if (new Date(events[0].start.dateTime) < from || new Date(events[0].end.dateTime) > to){
              callback('bigger', events)
            } else if (new Date(events[0].start.dateTime) > from || new Date(events[0].end.dateTime) < to){
              callback('smaller', events)
            } else {
              callback('equals', events)
            }
          } else {
            callback(false);
          }
        });
      });
    });
  }

  /**
   * Elimina eventi dal calendario
   * @param {*} event evento da eliminare
   * @param {*} callback fuzione da chiamare successivamente
   */
  function deleteEvent(chat_id, event, callback){
    fs.readFile(credentialsPath, (err, content) => {
      if (err) return console.log('Error loading client secret file:', err);
      authorize(JSON.parse(content), (auth) => {
        const calendar = google.calendar({version: 'v3', auth})
        calendar.events.delete({
          calendarId: calendarToken,
          eventId: event.id,
        }, function(err) {
          if (err) {
            googleRetry++;
            if (googleRetry < MAX_RETRIES){
              if (err.code == 403){
                console.log('Rate Limit Error, retry');
              } else {
                console.log('Delete event error: ' + err + ', retry');
              }
              setTimeout(deleteEvent, 3000, chat_id, event, callback);
            } else {
              if (googleRetry == MAX_RETRIES){
                const answer = "E' stato raggiunto il numero massimo di tentativi e non è stato possibile portare a termine l'operazione, riprova in un altro momento. \nAttenzione: nel caso di più giorni di permesso, è possibile che parte dei giorni siano stati eliminati.";
                data.tts[chat_id] ? sendAudio(answer, "richiesta_già_esaudita.mp3", chat_id, true) : bot.sendMessage(chat_id, answer);
              }
            } 
            return;
          }
          console.log('Event deleted.');
          callback(true);
        });
      });
    });
  }

  /**
   * Inserisce un evento nel calendario
   * @param {*} from inizio dell'evento
   * @param {*} to fine dell'evento
   * @param {*} color colore dell'evento, tutti gli eventi di un certo utente hanno lo stesso colore
   * @param {*} approvedReq richiesta a cui appartiene l'evento
   * @param {*} callback funzione da chiamare successivamente
   */
  function createEvent(chat_id, from, to, color, approvedReq, callback){
    fs.readFile(credentialsPath, (err, content) => {
      if (err) return console.log('Error loading client secret file:', err);
      authorize(JSON.parse(content), (auth) => {
        const calendar = google.calendar({version: 'v3', auth});
        //crea e aggiunge evento in calendario
        const last_name = approvedReq.user.hasOwnProperty('last_name') ? ' ' + approvedReq.user.last_name : ' ';
        const summary = 'Permesso di ' + approvedReq.user.first_name + last_name;
        var event = {
            'summary': summary,
            'description': `Permesso richiesto tramite Telegram Bot (utente ${approvedReq.user.id})`,
            'start': {
                'dateTime': `${from}`,
                'timeZone': 'Europe/Rome',
            },
            'end': {
                'dateTime': `${to}`,
                'timeZone': 'Europe/Rome',
            },
            "location": `${approvedReq.user.id}`,
            "colorId": color
        };
        calendar.events.insert({
        auth: auth,
        calendarId: calendarToken,
        resource: event,
        }, function(err, event) {
          if (err) {
            googleRetry++;
            if (googleRetry < MAX_RETRIES){
              if (err.code == 403){
                console.log('Rate Limit Error, retry');
              } else {
                console.log('Delete event error: ' + err + ', retry');
              }
              setTimeout(createEvent, 3000, chat_id, from, to, color, approvedReq, callback);
            } else {
              if (googleRetry == MAX_RETRIES){
                const answer = "E' stato raggiunto il numero massimo di tentativi e non è stato possibile portare a termine l'operazione, riprova in un altro momento.\nAttenzione: nel caso di più giorni di permesso, è possibile che parte dei giorni siano stati registrati.";
                data.tts[chat_id] ? sendAudio(answer, "richiesta_già_esaudita.mp3", chat_id, true) : bot.sendMessage(chat_id, answer);
              }
            } 
            return;
          }
          console.log('Event created.');
          callback(true);
        });
      });
    });
  }

/**
 * FUNZIONI PER GESTIRE L'APPROVAZIONE O IL RIFIUTO DELL'ADMIN
 */
  /** Risponde all'admin, notifica il dipendente, elimina la richiesta in quanto utilizzata, eventualmente inserisce l'evento nel calendario*/
  function adminResponse(cb_query){
    //controlla che la risposta sia stata mandata da un admin registrato nel json
    const chat_id = cb_query.from.id;
    if (data.admins.filter((a) => {return a == chat_id}).length == 1){
      //individua messaggio approvato e definire variabili di conseguenza
      const message_id = cb_query.message.message_id;
      const response = cb_query.data;
      const approvedReq = data.requests.filter(req => req.sentMessages.some(mx => mx.chat_id == chat_id && mx.message_id == message_id))[0];
      //se la richiesta non è presente, vuol dire che è già stata approvata/disapprovata
      if (approvedReq == undefined){
        const answer = "Questa richiesta è datata vi ha già risposto un amministratore.";
        data.tts[chat_id] ? sendAudio(answer, "richiesta_già_esaudita.mp3", chat_id, true) : bot.sendMessage(chat_id, answer);
        console.log("Final Response: " + answer);
      } else {
        const periodo = getPeriodo(approvedReq.date);
        const last_name = approvedReq.user.hasOwnProperty('last_name') ? ' ' + approvedReq.user.last_name : ' ';
        let color = '';
        if (data.colors.hasOwnProperty(approvedReq.user.id)){
          color = data.colors[approvedReq.user.id];
        } else {
          if (Object.keys(data.colors).length == 0){
            color = getRandomInt(1, 11);
          } else {
            color = ((data.colors[Object.keys(data.colors)[Object.keys(data.colors).length - 1]] + 1) % 11);
            console.log(data.colors[Object.keys(data.colors)[Object.keys(data.colors).length - 1]]);
            console.log(color);
          }
          data.colors[approvedReq.user.id] = color;
        }

        if (response == 'yAdmin'){
          const startDate = new Date(approvedReq.date.startDateTime);
          const endDate = new Date(approvedReq.date.endDateTime);
          //tutti i check presenti sono già stati effettuati al momento della conferma di richiesta da parte del'utete, 
          //sono ripetuti perchè fra il momento della richiesta e quello dell'approvazione potrebbero essere avvenute modifiche sul calendario
          listEvents(chat_id, startDate, endDate, (alreadyExists) =>{
            //controllo se l'user per questo giorno ha già registrato un permesso che ricopre anche la porzione di orario delle richiesta attuale
            if (startDate.getUTCDay() == endDate.getUTCDay() && (alreadyExists == 'equals' ||  alreadyExists == 'bigger')){
              notifyAdminAndUser(`Non è possibile registrare il permesso ${periodo} per il dipendente ${approvedReq.user.first_name + last_name} perchè già presente un permesso a suo nome.`,
              `Non è possibile procedere con la richiesta, hai già registrato un permesso a tuo nome ${periodo}.`, approvedReq, chat_id);
            } else {
              let creableEvents = 0;
              let eventsCreated = 0;
              const dates = getDates(startDate, endDate);
              if (dates.length > 60) {
                const answer = "L'operazione potrebbe richiedere un po' di tempo.";
                data.tts[chat_id] ? sendAudio(answer, "user_tempo_creazioen.mp3", chat_id, false) : bot.sendMessage(chat_id, answer)
              }
              dates.forEach((date) => {
                const start = dates.length != 1 ? new Date(date.setHours(MORNING_START.substring(0,2))) : startDate;
                const end = dates.length != 1 ? new Date(date.setHours(AFTERNOON_END.substring(0,2))) : endDate;
                listEvents(chat_id, start, end, (alreadyExists, events) =>{
                  if (!isFestivo(date)){
                    //se è già registrato un permesso che ricopre una porzione di orario della richiesta attuale, lo elimino per creare un permesso più esteso
                    if (alreadyExists == 'smaller'){
                      events.forEach((ev) => {
                        deleteEvent(chat_id, ev, (del) => {
                          if (del == false){
                            notifyAdminAndUser(`C'è stato un problema nell'eliminazione dei permessi già presenti per le date indicate, controlla il calendario.`,
                            `C'è stato un problema nell'eliminazione dei permessi già presenti per le date indicate, controlla il calendario.`, approvedReq, chat_id);
                          }
                        });
                      })
                    //ulteriore check di permesso più esteso in questa giornata già esistente
                    } else if ((alreadyExists == 'equals' || alreadyExists == 'bigger') && dates.length == 1){
                      notifyAdminAndUser(`Il permesso ${periodo} del dipendente ${approvedReq.user.first_name + last_name} non è stato registrato in quanto era già presente un permesso a suo nome in quel periodo.`,
                      `Il permesso ${periodo} non è stato registrato in quanto era già presente un permesso a tuo nome in quel periodo.`, approvedReq, chat_id);
                    }
                    //se non è presente alcun permesso o c'è un permesso di un porzione di tempo contenuta in quella della richiesa attuale, crea l'evento
                    if (!alreadyExists || alreadyExists == 'smaller'){
                      creableEvents++;
                      createEvent(chat_id, start.toISOString(), end.toISOString(), color, approvedReq, (created) => {
                        eventsCreated += created ? 1 : 0;
                        if (eventsCreated === creableEvents){
                          googleRetry = 0;
                          notifyAdminAndUser(`Ho notificato il dipendente ${approvedReq.user.first_name + last_name} che la sua richiesta di permesso ${periodo} è stata approvata.`,
                          `La tua richiesta di permesso ${periodo} è stata approvata.`, approvedReq, chat_id);
                        }
                      });
                    }
                  }
                }, approvedReq.user.id);
              });
            }
          },approvedReq.user.id);
        } else {
          notifyAdminAndUser(`Ho notificato il dipendente ${approvedReq.user.first_name + last_name} che la sua richiesta di permesso ${periodo} non è stata approvata.`,
          `ATTENZIONE! La tua richiesta di permesso ${periodo} NON è stata approvata.`, approvedReq, chat_id);
        }
      }
    }
  }

  /**Notifica l'amministratore e l'utente sull'esito finale della richista */
  function notifyAdminAndUser(msgAdmin, msgUser, approvedReq, userId){
    //notifica dipendente
    let answer = msgUser;
    data.tts[approvedReq.user.id] ? sendAudio(answer, "approvazione_permesso.mp3", approvedReq.user.id, false) : bot.sendMessage(approvedReq.user.id, answer).catch(error => {
      if (error) throw error;
      console.log(error.response.body);
    });
    console.log("Final Response User: " + answer);

    //risponde all'amministratore
    answer = msgAdmin;
    data.tts[userId] ? sendAudio(answer, "notifica_dipendente.mp3", userId, false) : bot.sendMessage(userId, answer);
    console.log("Final Response Admin: " + answer);

    //elimino tutti i messaggi inviati agli amministratori riguardanti questa richiesta
    approvedReq.sentMessages.forEach(mx => {
      bot.deleteMessage(mx.chat_id, mx.message_id).catch(error => {
        if (error) throw error;
        console.log(error.response.body);
      });
    });

    //elimina la richiesta
    const deletePos = data.requests.indexOf(data.requests.filter((r) => {return r.id == approvedReq.id})[0]);
    data.requests.splice(deletePos, 1);
    jsonWriter(data);
  }

/**
 * FUNZIONI TEXT TO SPEECH
 */
  /**
    * Crea un audio
    * @param {*} text il testo da convertire in audio
    * @param {*} fileName  nome del file nella cartella audio che conterrà l'audio creato
    * @param {*} callback funzione che verrà chiamata una volta creato l'audio
    * @param {*} isSSML indica se il testo contiene dei tag ssml
    */
  async function createAudio(text, fileName, callback=null, isSSML=false) {
    let jsonText = isSSML ? {ssml: text} : {text: text};
    const request = {
      input: jsonText,
      voice: {languageCode: 'it-IT', ssmlGender: 'NEUTRAL'},
      audioConfig: {audioEncoding: 'MP3'},
    };
    const [response] = await ttsClient.synthesizeSpeech(request);
    const writeFile = util.promisify(fs.writeFile);
    await writeFile(path.join(audioOutputPath, fileName), response.audioContent, 'binary');
    console.log('Audio content written to file: ' + fileName);
    callback();
  }

  /**
   * Invia un audio 
   * @param {*} text il testo da convertire in audio
   * @param {*} fileName nome del file nella cartella audio che conterrà l'audio creato
   * @param {*} userId utente a cui inviare l'audio
   * @param {*} isSSML indica se il testo contiene dei tag ssml
  */
  function sendAudio(text, fileName, userId, staticAudio, isSSML=false){
    try {
      if (staticAudio && fs.existsSync(path.join(audioOutputPath, fileName))) {
        bot.sendAudio(userId, path.join(audioOutputPath, fileName)).catch(error => {
          if (error) throw error;
          console.log(error.response.body);
        });
      } else {
        createAudio(text, fileName, () =>{
          bot.sendAudio(userId, path.join(audioOutputPath, fileName)).catch(error => {
            if (error) throw error;
            console.log(error.response.body);
          });
        }, isSSML);
      }
    } catch(err) {
      console.error(err)
    }
  }

/**
 * FUNZIONI SPEECH TO TEXT
 */
  /**Da un messaggio ricevuto su Telegram estrae il testo, che sia un messaggio testuale o vocale*/
  function getTextFromMessage(msg, callback){
    if (msg.hasOwnProperty('voice')){
      bot.downloadFile(msg.voice.file_id, audioInputPath).then((path) => {
        transcriptAudio(path, (text) => callback(text))}).catch(error => {
          console.log('Dowload File Error');
          console.log(error.code);
          console.log(error.response.body);
        });
    } else {
      const text = msg.text;
      callback(text);
    }
  }

  /**Da un messaggio vocale inviato su Telegram estrae il testo*/
  async function transcriptAudio(path, callback){
      const file = fs.readFileSync(path);
      const audioBytes = file.toString('base64');

      // The audio file's encoding, sample rate in hertz, and BCP-47 language code
      const request = {
        audio: {content: audioBytes},
        config: {
          encoding: 'OGG_OPUS',
          sampleRateHertz: 48000,
          languageCode: 'it-IT',
        }
      };

      // Detects speech in the audio file
      const [response] = await sttClient.recognize(request);
      const text = response.results
        .map(result => result.alternatives[0].transcript)
        .join('\n');
      fs.unlink(path, (err) => {if (err) throw err});
      callback(text);
  }

/**
 * Agisce in base all'intento individuato da Dialogflow
 * @param {*} telegramMsg messaggio originale ottenuto da Telegram
 * @param {*} detectedIntent intendo individuato
 * @param {*} parameters parametri individuati
 */
function fulfillment(telegramMsg, detectedIntent, parameters, request, response, sessionId){
  /** Nel caso l'intento non necessiti di specifiche istruzioni, si utilizza la riposta restituita da dialogflow*/
  function fallback(){
    data.tts[userId] ? sendAudio(response, "response.mp3", userId, false) : bot.sendMessage(userId, response);
    console.log("Final Response: " + response);
  }

  /** Risponde al saluto dell'utente*/
   function welcome(){
    const first_name = telegramMsg.chat.first_name;
    answer = `Ciao, ${first_name}!`;
    data.tts[userId] ? sendAudio(answer, "ciao.mp3", userId, false) : bot.sendMessage(userId, answer);
    console.log("Final Response: " + answer);
  }

  /* Lanciata all'avvio del bot*/
  function telegramWelcome(){
    const first_name = telegramMsg.chat.first_name;
    answer = `Ciao, ${first_name}, benvenuto in FerieBot. Puoi procedere con la richiesta.`;
    data.actualRequests = {};
    jsonWriter(data);
    data.tts[userId] ? sendAudio(answer, "benvenuto.mp3", userId, false) : bot.sendMessage(userId, answer);
    console.log("Final Response: " + answer);

  }

  /** Visualizza i comandi che l'utente può eseguire (diversi se amministratore o dipendente)*/
  function telegramSettings(){  
    const pause = data.tts[userId] ? '<break time="'+breaktime+'s"/>' : '';
    let fileName = '';
    answer = data.tts[userId] ? '<speak>':'';
    if (data.admins.filter((a) => {return a == userId}).length == 1){
      fileName = "admin_settings.mp3"
      answer += `Puoi utilizzare i seguenti comandi: ${pause}
/help per ottenere istruzioni sul funzionamento del bot ${pause}
/start per cominciare una nuova conversazione ${pause}
/user per sapere che tipo di utente sei ${pause}
/richieste per visualizzare tutte le richieste che ancora non hanno ricevuto risposta ${pause}
/riepilogo specificando un mese per visualizzare i permessi registrati sul calendario in tale mese ${pause}
/audio per ricevere messaggi vocali dal bot ${pause}
/text per ricevere messaggi di testo dal bot`
    }else {
      fileName = "user_settings.mp3"
      answer += `Puoi utilizzare i seguenti comandi: ${pause}
/help per ottenere istruzioni sul funzionamento del bot ${pause}
/start per cominciare una nuova conversazione ${pause}
/user per sapere che tipo di utente sei ${pause}
/riepilogo per visualizzare i tuoi permessi registrati sul calendario per il prossimo anno ${pause}
/audio per ricevere messaggi vocali dal bot ${pause}
/text per ricevere messaggi di testo dal bot`
    }
    answer += data.tts[userId]? '</speak>':'';
    bot.sendMessage(userId, answer);
    data.tts[userId] ? sendAudio(answer, fileName, userId, true) : null;
    console.log("Final Response: " + answer);
  }

  /**Inserisce il periodo richiesto dall'utente nel file json */
  function setPeriodo(mode, callback) {
    let askTimePeriod = "none";
    if (data.actualRequests.hasOwnProperty(userProp) && data.actualRequests[userProp].hasOwnProperty('askTimePeriod')){
      askTimePeriod = data.actualRequests[userProp].askTimePeriod;
      delete data.actualRequests[userProp].askTimePeriod;
    } else {
      const periodo = parameters.fields['periodo-permesso'].listValue.values.length != 0 ? 
          (parameters.fields['periodo-permesso'].listValue.values[0].hasOwnProperty('stringValue') ? 
            parameters.fields['periodo-permesso'].listValue.values[0].stringValue : parameters.fields['periodo-permesso'].listValue.values[0].structValue.fields) 
        : "";
      const quanto = parameters.fields.hasOwnProperty('quanto-tempo') && parameters.fields['quanto-tempo'].hasOwnProperty("structValue") ? 
                    parameters.fields['quanto-tempo'].structValue.fields : "";
      if ((quanto == "" || quanto == undefined) && !(periodo == "" || periodo == undefined)){  
        if (periodo.hasOwnProperty('endTime') || periodo.hasOwnProperty('endDateTime')){
          //es. permesso il 20 maggio dalle 9 alle 12
          const startString = periodo.hasOwnProperty('startTime') ? 'startTime' : 'startDateTime';
          const endString = periodo.hasOwnProperty('endTime') ? 'endTime' : 'endDateTime';
          data.actualRequests[userProp] = {date:''};
          data.actualRequests[userProp].date = getRightTime({
            startTime: periodo[startString].stringValue,
            endTime: periodo[endString].stringValue,
          });
        } else if (periodo.hasOwnProperty('endDate')){
          //es. permesso dal 20 al 23 maggio
          const start = JSON.stringify(periodo.startDate.stringValue).substring(1, 11);
          const end = JSON.stringify(periodo.endDate.stringValue).substring(1, 11);
          data.actualRequests[userProp] = {
            date:{
              endDateTime: end + 'T'+ AFTERNOON_END + TIMEZONE,
              startDateTime: start + 'T'+ MORNING_START + TIMEZONE
            }
          }
        } else {
          let giorno = '';
          let json = '';
          if (periodo.hasOwnProperty("date_time")){
            //es. permesso domani alle 10
            giorno = periodo.date_time.stringValue;
            json = {
              endDateTime: giorno,
              startDateTime: giorno
            }
          } else {
            //es. permesso il 10 maggio
            giorno = JSON.stringify(periodo);
            json = {
              endDateTime: giorno.substring(1, 11) + 'T'+ AFTERNOON_END + TIMEZONE,
              startDateTime: giorno.substring(1, 11) +'T'+ MORNING_START + TIMEZONE
            }
          }
          if (!data.actualRequests.hasOwnProperty(userProp)){
            data.actualRequests[userProp] = {
              date: json
            }
          } else {
            data.actualRequests[userProp].date = json;
          }
          askTimePeriod = periodo.hasOwnProperty("date_time") ? 'quanto' : (data.actualRequests[userProp].hasOwnProperty("quanto") ? "da-ora" : "dalle-alle");
        }  
      } else if (!(quanto == "" || quanto == undefined)) {
        console.log('indicazione di tempo');
        const startTime = new Date(quanto.startDateTime.stringValue); 
        const endTime = new Date(quanto.endDateTime.stringValue);
        let diff = endTime.getTime() - startTime.getTime();
        if (periodo == "" || periodo == undefined){
          if (diff >= MILLISEC_DAY){
            //es. permesso di 3 giorni
            askTimePeriod = "da-giorno";
          } else {
            //es. permesso di 3 ore
            askTimePeriod = "in-giorno";
          }
        } else {
          if (periodo.hasOwnProperty("startDate")){
            //es. permesso di 3 giorni dal 20 al 22 maggio
            const startPeriodo = new Date(periodo.startDate.stringValue); 
            const endPeriodo = new Date(periodo.endDate.stringValue);
            startPeriodo.setHours(9);
            endPeriodo.setHours(18, 0, 0, 0);
            data.actualRequests[userProp] = {
              date: {
                endDateTime: dateToString(endPeriodo),
                startDateTime: dateToString(startPeriodo)
              }
            }
          } else if (periodo.hasOwnProperty("startDateTime")){
            const startPeriodo = new Date(periodo.startDateTime.stringValue); 
            const endPeriodo = new Date(periodo.endDateTime.stringValue);
            startPeriodo.setHours(9);
            endPeriodo.setHours(18, 0, 0, 0);
            data.actualRequests[userProp] = {
              date:{
                endDateTime: dateToString(endPeriodo),
                startDateTime: dateToString(startPeriodo)
              }
            }
            askTimePeriod = "da-ora";
          } else {
            const startDate = periodo.hasOwnProperty('date_time') ? new Date(periodo.date_time.stringValue): new Date(periodo);
            let endDate = '';
            if (diff >= MILLISEC_DAY){
              //es. permesso di 3 giorni da domani
              startDate.setHours(9);
              diff -= MILLISEC_DAY;
              endDate = new Date(new Date(periodo).setHours(9) + diff);
              endDate.setHours(18);
            } else if (startDate.getHours() != 12){
              //es. permesso di 1 ora oggi alle 10
              endDate = new Date(startDate);
              endDate = new Date(endDate.setMilliseconds(startDate.getMilliseconds() + diff));
            } else {
              //es. permesso di 3 ore domani
              endDate = startDate;
              askTimePeriod = "da-ora";
            }
            data.actualRequests[userProp] = {
              date:{
                startDateTime: getRightHour(dateToString(startDate)),
                endDateTime: getRightHour(dateToString(endDate))
              }
            }
          }
        }
        if (!data.actualRequests.hasOwnProperty(userProp)){
          data.actualRequests[userProp] = {};
        }
        data.actualRequests[userProp].quanto = diff;
      }
      if((askTimePeriod == 'da-ora' || askTimePeriod == 'quanto' || askTimePeriod == 'dalle-alle' || askTimePeriod == 'none') && new Date(data.actualRequests[userProp].date.startDateTime) < new Date()){
        data.actualRequests[userProp].askTimePeriod = askTimePeriod;
        askTimePeriod = 'passato';
      } else {
        delete data.actualRequests[userProp].askTimePeriod ;
      }
      data.actualRequests[userProp].mode = mode;
    }

    //check giorno/i festivo/i
    if (data.actualRequests.hasOwnProperty(userProp) && data.actualRequests[userProp].hasOwnProperty('date')){
      const dates = getDates(new Date(data.actualRequests[userProp].date.startDateTime), new Date(data.actualRequests[userProp].date.endDateTime));
      let festivi = 0;
      dates.forEach((date) => {festivi += isFestivo(date)? 1 : 0;});
      if (festivi == dates.length){
        answer = dates.length == 1 ? "Non è possibile procedere con la richiesta, il giorno selezionato è un festivo.": "Non è possibile procedere con la richiesta, i giorni selezioni sono dei festivi.";
        data.tts[userId] == true ? sendAudio(answer, fileName, userId, fileName == "conferma.mp3" ? false : true ) : bot.sendMessage(userId, answer);
      } else {
        jsonWriter(data);
        callback(data.actualRequests.hasOwnProperty(userProp)? data.actualRequests[userProp].date : null, askTimePeriod);
      }
    } else {
      jsonWriter(data);
      callback(data.actualRequests.hasOwnProperty(userProp)? data.actualRequests[userProp].date : null, askTimePeriod);
    }
  }

  async function setContextQuery(contextName, lifespanCount = 2){
    const context = await createContext(sessionId, contextName, {}, lifespanCount);
    await sendQuery(sessionId, request, context);
  }

  /** Inserisce un periodo in modalità creazione e in base cosa manca per */
  function setPeriodoCreate() {
    setPeriodo('create', async (newPeriodo, askTimePeriod) => {
      let context = '';
      //se ha inserito l'orario o ha inserito un periodo con più date chiede la conferma, altrimenti chiede se vuole inserire l'orario/il giorno/la quantità di tempo 
      if (askTimePeriod == 'dalle-alle'){
        context = 'inserimento-orario';
        answer = 'Vuoi specificare un orario?';
        fileName = "richiesta_orario.mp3";
      } else if (askTimePeriod == 'da-ora') {
        context = 'inserimento-orario-da';
        answer = 'Da che ora?';
        fileName = "richiesta_orario_da.mp3";
      } else if (askTimePeriod == 'da-giorno'){
        context = 'data-permesso-da';
        answer = 'Da che giorno?';
        fileName = "richiesta_data_da.mp3";
      } else if (askTimePeriod == 'in-giorno'){
        context = 'data-permesso';
        answer = 'In che giorno?';
        fileName = "richiesta_data.mp3"; 
      } else if (askTimePeriod == 'quanto'){
        context = 'quanto-tempo';
        answer = 'Di quanto tempo?';
        fileName = "richiesta_quanto_tempo.mp3"; 
      } else if (askTimePeriod == 'passato'){
        context = 'passato';
        answer = 'La data che hai richiesto è già passata, sei sicuro di voler procedere? (Si/No)';
        fileName = "richiesta_passata.mp3"; 
      }else {
        context = 'conferma-permesso';
        answer = `Confermi di voler richiedere un permesso ${getPeriodo(newPeriodo)}? (Si/No)`;
        fileName = "conferma.mp3";
      }
      setContextQuery(context);
      data.tts[userId] == true ? sendAudio(answer, fileName, userId, fileName == "conferma.mp3" ? false : true ) : bot.sendMessage(userId, answer);
      console.log("Final Response: " + answer);
    });
  }

  /** Inserisce un periodo in modalità eliminazione */
  function setPeriodoDelete() {
    if (parameters.fields['periodo-permesso'].listValue.values.length == 0){
      fallback();
    } else {
      setPeriodo('delete', (newPeriodo, tts) => {
      
        answer = newPeriodo ? `Cancellerò permessi ${getPeriodo(newPeriodo)}, va bene?`:'Probabilmente hai fatto un errore ortografico, prova a riformulare la richiesta prestando attenzione.';
        tts[userId] ? sendAudio(answer, "conferma_cancellazione.mp3", userId, false) : bot.sendMessage(userId, answer);
        console.log("Final Response: " + answer);
      });
    }
  }

  /** Nel caso in cui siano stati inseriti un periodo di tempo in termini di giorni e una data di partenza modifica adeguatamente il file json e chiede conferma*/
  function dataDa(){
    const giornoDa = parameters.fields['data-da'].stringValue;
    if(data.actualRequests.hasOwnProperty(userProp) && data.actualRequests[userProp].hasOwnProperty("quanto")){
      console.log("setData"); 
      const startDate = new Date(giornoDa);
      startDate.setHours(9);
      data.actualRequests[userProp].quanto -= MILLISEC_DAY;
      const endDate = new Date(new Date(startDate.setHours(9) + data.actualRequests[userProp].quanto));
      endDate.setHours(18);
      data.actualRequests[userProp].date = {
        startDateTime: dateToString(startDate),
        endDateTime: dateToString(endDate)
      }
      delete data.actualRequests[userProp].quanto;
      jsonWriter(data);
      
      setContextQuery('conferma-permesso')
      answer = `Confermi di voler richiedere un permesso ${getPeriodo(data.actualRequests[userProp].date)}? (Si/No)`;
      data.tts[userId] ? sendAudio(answer, "conferma.mp3", userId, false) : bot.sendMessage(userId, answer);
      console.log("Final Response: " + answer);
    } else {
      console.log('setData errore: no actualRequest or no quanto');
    }
  }

  /** Nel caso in cui siano state inserite una data, un periodo di tempo in termini di ore e un orario di partenza modifica adeguatamente il file json e chiede conferma*/
  function orarioDa(){
    //controlla se esista già una richiesta attuale: questa funzione agisce solo sull'orario
    if(data.actualRequests.hasOwnProperty(userProp)){
      const start = parameters.fields.hasOwnProperty('orario-da') ? getRightHour(data.actualRequests[userProp].date.startDateTime.substring(0,10) + parameters.fields['orario-da'].stringValue.substring(10,25)) : data.actualRequests[userProp].date.startDateTime;
      let end='';
      if (parameters.fields.hasOwnProperty('quanto') && parameters.fields.quanto.hasOwnProperty('stringValue')){
        end = getRightHour(data.actualRequests[userProp].date.startDateTime.substring(0,10) + parameters.fields['quanto'].stringValue.substring(10,25)).substring(0,19) + TIMEZONE;
      } else {
        let quanto = '';
        if (parameters.fields.hasOwnProperty('quanto')) {
          const startTime = new Date(parameters.fields.quanto.structValue.fields.startDateTime.stringValue); 
          const endTime = new Date(parameters.fields.quanto.structValue.fields.endDateTime.stringValue);
          quanto = endTime.getTime() - startTime.getTime();
        } else {
          quanto = data.actualRequests[userProp].quanto
        }
        const endDateTime =  new Date(start.substring(0,19));
        endDateTime.setMilliseconds(endDateTime.getMilliseconds() + quanto);
        end = getRightHour(dateToString(endDateTime)).substring(0,19) + TIMEZONE;
      }
      data.actualRequests[userProp].date.startDateTime = start;
      data.actualRequests[userProp].date.endDateTime = end;
      delete data.actualRequests[userProp].quanto;
      jsonWriter(data);
      
      setContextQuery('conferma-permesso');
      answer = `Confermi di voler richiedere un permesso ${getPeriodo(data.actualRequests[userProp].date)}? (Si/No)`;
      data.tts[userId] ? sendAudio(answer, "conferma.mp3", userId, false) : bot.sendMessage(userId, answer);
      console.log("Final Response: " + answer);
    } else {
      console.log('setOrario errore: no actualRequest');
    }
  }

  /** Modifica la data inserita in precedenza aggiungendovi l'orario e chiede conferma*/
  function orarioToUpdate(){
    if (parameters.fields['periodo-orario'].hasOwnProperty('structValue')){
      const orario = parameters.fields['periodo-orario'].structValue.fields;
      //controlla se esista già una richiesta attuale: questa funzione agisce solo sull'orario
      if(data.actualRequests.hasOwnProperty(userProp)){
        data.actualRequests[userProp].date = getRightTime({
          startTime: data.actualRequests[userProp].date.startDateTime.substring(0,10) + orario.startTime.stringValue.substring(10,25),
          endTime: data.actualRequests[userProp].date.endDateTime.substring(0,10) + orario.endTime.stringValue.substring(10,25)
        });
        jsonWriter(data);
        
        setContextQuery('conferma-permesso');
        answer = `Confermi di voler richiedere un permesso ${getPeriodo(data.actualRequests[userProp].date)}? (Si/No)`;
        data.tts[userId] ? sendAudio(answer, "conferma.mp3", userId, false) : bot.sendMessage(userId, answer);
        console.log("Final Response: " + answer);
      } else {
        console.log('setOrario errore: no actualRequest');
      }
    } else if (parameters.fields['momento-orario'].hasOwnProperty('stringValue') && parameters.fields['momento-orario'].stringValue != ''){ 
      const momento = parameters.fields['momento-orario'].stringValue;
      if(data.actualRequests.hasOwnProperty(userProp)){
        data.actualRequests[userProp].date = getRightTime({
          startTime: data.actualRequests[userProp].date.startDateTime.substring(0,10) + momento.substring(10,25),
          endTime: data.actualRequests[userProp].date.endDateTime.substring(0,10) + momento.substring(10,25)
        });
        jsonWriter(data);

        setContextQuery('quanto-tempo');
        answer = 'Di quanto tempo?';
        data.tts[userId] ? sendAudio(answer, "richiesta_quanto_tempo.mp3", userId, true) : bot.sendMessage(userId, answer);
        console.log("Final Response: " + answer);
      } else {
        console.log('setOrario errore: no actualRequest');
      }
    } else {
      setContextQuery('inserimento-orario');
      fallback();
    }
  }

  /** L'orario non è da aggiornare, chiede solamente conferma della data */
  function orarioNotToUpdate(){
    answer = `Confermi di voler richiedere un permesso ${getPeriodo(data.actualRequests[userProp].date)}? (Si/No)`;
    data.tts[userId] ? sendAudio(answer, "conferma.mp3", userId, false) : bot.sendMessage(userId, answer);
    console.log("Final Response: " + answer);
  }

  /** Se l'utente non conferma di voler procedere con la richiesta, la elimina*/
  function deleteActualRequest(){
    delete data.actualRequests[userProp];
    jsonWriter(data);
    fallback();
  }

  /** Aggiorna i sentMessages inviati agli admin per una certa richiesta */
  function updateRequest(msg, actualRequest, userProp){
    if (!actualRequest.hasOwnProperty('sentMessages')){
      actualRequest.sentMessages = JSON.parse('[]');
    }
    //registra il messaggio inviato nella richiesta per individuarla una volta che l'admin risponderà al messaggio
    actualRequest.sentMessages.push({"chat_id":msg.chat.id,"message_id":msg.message_id});
    if (actualRequest.adminId == data.admins.length-1){
      data.requests.push(actualRequest);
      delete data.actualRequests[userProp];
      jsonWriter(data);
    }
    actualRequest.adminId++;
  }

  /** Nel momento in cui la richiesta è stata confermata dall'utente, la invia effettivamente dall'admin o la registra come da inviare in futuro */
  function userRequest(){
    const first_name = telegramMsg.chat.first_name;
    data.actualRequests[userProp].user = {"id":userId, "first_name":first_name};
    const last_name = telegramMsg.chat.last_name;
    if (last_name != undefined){
      data.actualRequests[userProp].user.last_name = last_name;
    }
    const mode = data.actualRequests[userProp].mode;

    //se si tratta di un inserimento, notifica gli amministratori
    if (mode == 'create'){
      const startDate = new Date(data.actualRequests[userProp].date.startDateTime);
      const endDate = new Date(data.actualRequests[userProp].date.endDateTime);
      listEvents(userId, startDate, endDate, (alreadyExists) =>{
        //controlla che l'utente non abbia già inserito un permesso in quel periodo di tempo
        if (startDate.getUTCDay() == endDate.getUTCDay() && (alreadyExists == 'equals' ||  alreadyExists == 'bigger')){
          answer = 'Non è possibile procedere con la richiesta, hai già registrato un permesso a tuo nome per questo periodo di tempo.';
          data.tts[userId] ? sendAudio(answer, "notifica_user_gia_registrato.mp3", userId, true) : bot.sendMessage(userId, answer);
          console.log("Final Response: " + answer);
        } else {
          //controlla che il giorno indicato non sia festivo
          if (startDate.getUTCDay() == endDate.getUTCDay() && isFestivo(startDate)){
            answer = `Non posso procedere con la richiesta perchè il giorno selezionato è un festivo.`
            data.tts[userId] ? sendAudio(answer, "notifica_user_festivo.mp3", userId, true) : bot.sendMessage(userId, answer);
            console.log("Final Response: " + answer);
          } else {
            const dates = getDates(startDate, endDate);
            let creableEvents = 0;
            dates.forEach((date, idx, array) => {
              const start = dates.length != 1 ? new Date(date.setHours(MORNING_START.substring(0,2))) : startDate;
              const end = dates.length != 1 ? new Date(date.setHours(AFTERNOON_END.substring(0,2))) : endDate;
              listEvents(userId, start, end, (alreadyExists) =>{
                //controlla quanti sono gli eventi creabili (la data non è un festivo e non sono già presenti permessi dell'utente in quel periodo di tempo)
                if (!isFestivo(date)){
                  if (!alreadyExists || alreadyExists == 'smaller'){
                    creableEvents++;
                  }
                }
                if (idx === array.length - 1){ 
                  if (creableEvents == 0){
                    answer = `Non è possibile procedere con la richiesta, hai già registrato un permesso a tuo nome per questo periodo di tempo.`
                    data.tts[userId] ? sendAudio(answer, "notifica_user_gia_registrato.mp3", userId, true) : bot.sendMessage(userId, answer);
                    console.log("Final Response: " + answer);
                  } else {
                    if (data.admins.length != 0){
                      const actualRequest = Object.assign({}, data.actualRequests[userProp]);
                      if (data.requests.length >= 1)
                        actualRequest.id = data.requests.sort((a, b) => { return parseFloat(b['id']) - parseFloat(a['id'])})[0]['id'] + 1;
                      else {
                        actualRequest.id = 1;
                      }
                      actualRequest.adminId = 0;
                      answer = getMessage(data.actualRequests[userProp], "create");
                      //manda messaggi di richiesta a tutti gli admin
                      data.admins.forEach((admin) =>{
                        fileName = 'richiesta.mp3'; 
                        if (data.tts[admin]){
                          createAudio(answer, fileName, () => {
                            bot.sendAudio(admin, path.join(audioOutputPath, fileName), approveOpts)
                            .then(msg => updateRequest(msg, actualRequest, userProp)).catch(error => {
                              console.log(error.code);
                              console.log(error.response.body);
                            });
                          });
                        } else {
                          bot.sendMessage(admin, answer, approveOpts)
                          .then(msg => updateRequest(msg, actualRequest, userProp)).catch(error => {
                            if (error) throw error;
                            console.log(error.response.body);
                          });
                        }
                      }); 
                      answer = "Ok, ho notificato gli amministratori.";
                      data.tts[userId] ? sendAudio(answer, "notifica_amministratori.mp3", userId, true) : bot.sendMessage(userId, answer);
                      console.log("Final Response: " + answer);
                    } else {
                      answer = "Non è ancora stato nominato un admin, prova a fare la richiesta di permesso più tardi.";
                      data.tts[userId] ? sendAudio(answer, "no_admin.mp3", userId, true) : bot.sendMessage(userId, answer);
                      console.log("Final Response: " + answer);
                    }
                  }
                }
              }, userId);
            });
          }
        }
      }, userId);
    }

    //se si stratta di una cancellazione, si autta direttamente
    else if (mode == "delete"){
      const startDate = new Date(data.actualRequests[userProp].date.startDateTime);
      const endDate = new Date(data.actualRequests[userProp].date.endDateTime);
      const daysInBetween = (endDate.getTime() - startDate.getTime()) / (1000 * 3600 * 24);
      listEvents(userId, startDate, endDate, (ex, events) =>{
        if (ex) {
          if (events.length > 15){
            answer = "L'operazione potrebbe richiedere un po' di tempo.";
            data.tts[userId] ? sendAudio(answer, "user_tempo_cancellazione.mp3", userId, false) : bot.sendMessage(userId, answer);
          }
          let deletedCount = 0;
          events.forEach((dEvent) => {
            deleteEvent(userId, dEvent, (deleted) => {
              if (deleted){
                deletedCount++;
                if (deletedCount == events.length){
                  googleRetry = 0;
                  let answerUser = events.length > 1 ? "I giorni di permesso sono stati cancellati.":"Il permesso è stato cancellato.";
                  if (events.length < daysInBetween){
                    const addNotice = "\nAttenzione: nel periodo di tempo indicato erano presenti giorni festivi o in cui non erano registrati permessi a tuo nome.";
                    answerUser += addNotice;
                  }
                  //avvisa l'utente
                  data.tts[userId] ? sendAudio(answerUser, "user_permesso_cancellato.mp3", userId, false) : bot.sendMessage(userId, answerUser);
                  console.log("Final Response User: " + answerUser);

                  //avvisa gli admin
                  const answerAdmin = getMessage(data.actualRequests[userProp], "delete");
                  data.admins.forEach(admin => {
                    if (data.tts[admin]) {
                      sendAudio(answerAdmin, "admin_permessso_cancellato.mp3", admin, false);
                    } else {
                      bot.sendMessage(admin, answerAdmin).catch(error => {
                        if (error) throw error;
                        console.log(error.response.body);
                      }); 
                    }
                  });
                  console.log("Final Response Admin: " + answerAdmin);

                  delete data.actualRequests[userProp];
                  jsonWriter(data);
                }
              }
            })
          });
        } else {
          answer = "Non c'è nessun permesso a tuo nome per il periodo indicato.";
          data.tts[userId] ? sendAudio(answer, "nessun_permesso.mp3", userId, true) : bot.sendMessage(userId, answer);
          console.log("Final Response: " + answer);
        }
      }, userId);
    }
  }

  /** Invia nuovamente le richieste ancora in sospeso all'admin che ne ha fatto richiesta*/
  function sendRequests(){
    if (data.admins.filter((a) => {return a == userId}).length == 1){
      if (data.requests.length >= 1) {
        data.requests.forEach(req => {
          answer = getMessage(req, "create");
          if (data.tts[userId]){
            fileName = "richieste_in_sospeso.mp3"
            createAudio(answer, fileName, () => {
              bot.sendAudio(userId, path.join(audioOutputPath, fileName), approveOpts)
              .then(msg => {
                req.sentMessages.push({"chat_id":msg.chat.id,"message_id":msg.message_id});
                jsonWriter(data);
              }).catch(error => {
                console.log(error.code);
                console.log(error.response.body);
              });
            });
          } else {
            bot.sendMessage(userId, answer, approveOpts)
            .then(msg => {
              req.sentMessages.push({"chat_id":msg.chat.id,"message_id":msg.message_id});
              jsonWriter(data);
            }).catch(error => {
              console.log(error.code);
              console.log(error.response.body);
            });
          }
          console.log("Final Response: " + answer);
        });
      } else {
        answer = "Non ci sono richieste in sospeso.";
        data.tts[userId] ? sendAudio(answer, "no_richieste.mp3", userId, true) : bot.sendMessage(userId, answer);
        console.log("Final Response: " + answer);
      }
    } else {
      answer = "Non sei un amministratore.";
      data.tts[userId] ? sendAudio(answer, "no_admin.mp3", userId, true) : bot.sendMessage(userId, answer);
      console.log("Final Response: " + answer);
    }
  }

  /** Genera il riepilogo per un user: tutti i suoi permessi del prossimo anno */
  function riepilogo(){
    const today = new Date();
    intentMap.set('Telegram Welcome', telegramWelcome);

    //se il riepilogo è stato richiesto da un admin, visualizza tutti i permessi nel mese richiesto
    if (data.admins.filter((a) => {return a == userId}).length == 1) {
      const mese = parameters.fields.mese.listValue.values.length == 0 ? "" : parameters.fields.mese.listValue.values[0].structValue.fields;
      let to = '';
      let from = '';
      if (mese == "" || mese == undefined){
        const y = new Date().getFullYear();
        const  m = new Date().getMonth() - 1;
        from = new Date(new Date().setFullYear(y, m, 0));
        to = new Date(new Date().setFullYear(y, m + 1, 0));
      } else {
        to = new Date(mese.endDate.stringValue);
        from = new Date(mese.startDate.stringValue);
      }
      const periodoMap = new Map();
      const namesMap = new Map();
      listEvents(userId, from, to, (exists, events) => {
        let riepilogo = data.tts[userId] ? '<speak>' : '';
        riepilogo += `Riepilogo dal ${getDate(from.addDays(1).toISOString())} al ${getDate(to.toISOString())}: `;
        riepilogo += data.tts[userId] ? '<break time="'+breaktime+'s"/>' : '\n \n';
        if (exists){
          //formatta ogni evento ad una stringa standard
          events.forEach(function(event) {
            let periodo = { 
              startDateTime: event.start.dateTime,
              endDateTime: event.end.dateTime
            };
            if (!periodoMap.has(event.location)){
              periodoMap.set(event.location, new Array());
              namesMap.set(event.location, event.summary.substring(12));
            } 
            (periodoMap.get(event.location)).push(periodo);
          });
          periodoMap.forEach((val, key) => {
            riepilogo += `Permessi di ${namesMap.get(key)}:`
            riepilogo += data.tts[userId] ? '<break time="'+breaktime+'s"/>' : '\n';
            val.forEach(p => {
              riepilogo += `${getWeekDay((new Date(p.startDateTime)).getDay())} ${getPeriodo(p, false).substring(3)}`;
              riepilogo += data.tts[userId] ? '<break time="'+breaktime+'s"/>' : '\n';
            });
            riepilogo += data.tts[userId] ? '<break time="'+breaktime+'s"/>' : '\n';
          });
          riepilogo += data.tts[userId] ? '</speak>' : '';
          data.tts[userId] ? sendAudio(riepilogo, "riepilogo.mp3", userId, false) : bot.sendMessage(userId, riepilogo);
        } else {
          riepilogo += "Non ci sono permessi in questo periodo";
          data.tts[userId] ? sendAudio(riepilogo, "riepilogo.mp3", userId, false) : bot.sendMessage(userId, riepilogo);
        }
        console.log("Final Response: " + riepilogo);
      });
    }
    //se invece è stato richiesto a un dipendente, visualizza i suoi permessi nel prossimo anno 
    else {
      const to = (new Date(new Date().setMonth(today.getMonth()+12)));
      listEvents(today, to, (exists, events) => {
        let riepilogo = data.tts[userId] ? '<speak>' : '';
        riepilogo += `Riepilogo dal ${getDate(today.toISOString())} al ${getDate(to.toISOString())}: \n \n`;
        riepilogo += data.tts[userId] ? '<break time="'+breaktime+'s"/>' : '';
        if (exists){
          events.forEach(function(event) {
            let periodo = {
              startDateTime: event.start.dateTime,
              endDateTime:event.end.dateTime
            };
            riepilogo +=  `${getPeriodo(periodo, false).substring(3)} \n`;
            riepilogo += data.tts[userId] ? '<break time="'+breaktime+'s"/>' : '';
          });
          riepilogo += data.tts[userId] ? '</speak>' : '';
          data.tts[userId] ? sendAudio(riepilogo, "riepilogo.mp3", userId, false) : bot.sendMessage(userId, riepilogo);
        } else {
          riepilogo += "Non ci sono permessi a tuo nome in questo periodo";
          data.tts[userId] ? sendAudio(riepilogo, "riepilogo.mp3", userId, false) : bot.sendMessage(userId, riepilogo);
        }
        console.log("Final Response: " + answer);
      }, userId);
    }
  }

  /** Controlla se l'utente è un amministatore gli risponde di conseguenza */
  function checkUser(){
    let fileName = '';
    if (data.admins.filter((a) => {return a == userId}).length == 1){
      fileName='user_admin.mp3';
      answer = `Sei registrato come amministratore (id ${userId}).`;
    } else {
      fileName='user_user.mp3';
      answer = `Sei registrato come dipendente (id ${userId}).`;
    }
    data.tts[userId] ? sendAudio(answer, fileName, userId, true) : bot.sendMessage(userId, answer);
    console.log("Final Response: " + answer);
  }

  /** Modifica il file json per far rispondere il bot tramite messaggi vocali */
  function setAudio(){
    data.tts[userId] = true;
    jsonWriter(data);
    bot.sendMessage(userId, response);
    console.log("Final Response: " + answer);
  }

  /** Modifica il file json per far rispondere il bot con messaggi testuali */
  function setText(){
    data.tts[userId] = false;
    jsonWriter(data);
    bot.sendMessage(userId, response);
    console.log("Final Response: " + answer);
  }

  function checkFallbacks(callback){
    if (!data.fallbacks.hasOwnProperty(sessionId)){
      data.fallbacks[sessionId] = 0;
    } else {
      if (detectedIntent == 'Default Fallback Intent') {
        data.fallbacks[sessionId]++;
        if (data.fallbacks[sessionId] >= MAX_FALLBACKS){
          response = "Sembra che non riesca a capirti, prova a ripetere la richiesta dall'inizio.\nPuoi usare il comando /more per vedere alcuni esempi di richieste.";
          data.fallbacks[sessionId] = 0;
        };
      } else {
        data.fallbacks[sessionId] = 0;
      }
    }
    callback();
    jsonWriter(data);
  }

  console.log('Telegram Message: ' + JSON.stringify(telegramMsg));
  console.log('Intent: ' + detectedIntent);
  console.log('Parameters: ' + JSON.stringify(parameters));
  console.log('Default Response: ' + response);
  const userId = telegramMsg.chat.id;
  let answer = '';
  let fileName = '';
  const userProp = 'user' + userId;
  const intentMap = new Map();
  intentMap.set('Telegram Welcome', telegramWelcome);
  intentMap.set('Telegram Settings', telegramSettings);
  intentMap.set('Default Welcome Intent', welcome);
  intentMap.set('user-richiesta-completa', setPeriodoCreate);
  intentMap.set('user-cancella-completo', setPeriodoDelete);
  intentMap.set('user-data-permesso', setPeriodoCreate);
  intentMap.set('user-check-data-passata.yes', setPeriodoCreate);
  intentMap.set('user-data-permesso-da', dataDa);
  intentMap.set('user-inserimento-orario.yes', orarioToUpdate);
  intentMap.set('user-inserimento-orario-da', orarioDa);
  intentMap.set('user-inserimento-quanto', orarioDa);
  intentMap.set('user-inserimento-orario.no', orarioNotToUpdate);
  intentMap.set('user-conferma-richiesta.yes', userRequest);
  intentMap.set('user-cancella-conferma.yes', userRequest);
  intentMap.set('user-conferma-richiesta.no', deleteActualRequest);
  intentMap.set('user-cancella-conferma.no', deleteActualRequest);
  intentMap.set('user-check-data-passata.no', deleteActualRequest);
  intentMap.set('admin-richieste', sendRequests);
  intentMap.set('check-user', checkUser);
  intentMap.set('riepilogo', riepilogo);
  intentMap.set('set-audio', setAudio);
  intentMap.set('set-text', setText);
  
  if(!data.tts.hasOwnProperty(userId)){
    data.tts[userId] = false;
  }
  if(data.admins.length == 0){
    bot.sendMessage(userId, "Non sono stati nominati amministratori, il bot attualmente non è utilizzabile.");
  } else {
    let intentInMap = false;
    intentMap.forEach((fun, intent) => {
      if (intent == detectedIntent) {
        fun();
        intentInMap = true;
      }
    });
    checkFallbacks(() => {intentInMap ? null : fallback();});
  }
}

/**
 * Aggiungo o elimina userId
 */
function setAdmin (userId, text){
    let answer = '';
    if (text == '/MEADMIN'){
      if (data.admins.filter((a) => {return a == userId}).length == 0){
        data.admins.push(userId);
        data.tts[userId] = false;
        answer = 'Sei stato registrato come admin.';
      } else {
        answer = 'Eri già registrato come admin.';
      }
    } else {
      if (data.admins.filter((a) => {return a == userId}).length == 1){
        data.admins.splice(data.admins.indexOf(userId), 1);
        answer = 'Sei stato eliminato dagli admin.';
      } else {
        answer = 'Non eri registrato come admin.';
      }
    }
    data.tts[userId] ? sendAudio(answer, "make_admins.mp3", userId, false) : bot.sendMessage(userId, answer);
    console.log("Final Response: " + answer);
    jsonWriter(data);
}

/**Quando il bot riceve un messaggio, lo trasforma in testo per necessario, lo invia a Dialogflow e in base alla risposta ottenuta chiama il fulfillment */
bot.on('message', function onMessage(msg) {
  googleRetry = 0;
  getTextFromMessage(msg, async function(text){
    console.log(`${text} (chat ${msg.chat.id})`);
    if (text == ''){
      bot.sendMessage(msg.chat.id, "Non è stato possibile rilevare il messaggio vocale proveniente da questo dispositivo. \nCambia dispositivo o invia un messaggio testuale.");
    } else if (text == '/MEADMIN' || text == '/STOPMEADMIN'){
      setAdmin(msg.chat.id, text);
    } else {
      if (data != ''){
        if (!data.sessionIds.hasOwnProperty(msg.chat.id)){
          data.sessionIds[msg.chat.id] = uuid.v4();
          jsonWriter(data);
        }
        const responses = await sendQuery(data.sessionIds[msg.chat.id], text);
        const result = responses[0].queryResult;
        if (result.intent) {
          fulfillment(msg, result.intent.displayName, result.parameters, text, result.fulfillmentText, data.sessionIds[msg.chat.id])
        } else {
          console.log(`  No intent matched.`);
        }
      }
    }
  })
}, (error) => {
  var time = new Date();
  console.log('OnMessage error')
	console.log("TIME:", time);
	console.log("CODE:", error.code);  // => 'EFATAL'
	console.log("MSG:", error.message);
	console.log("STACK:", error.stack);
  console.log("RESPONSE BODY:", error.response.body);
});

/**Quando il bot riceve una callback_query vuol dire che un admin ha risposto a una richiesta quindi chiama la funzione adeguata */
bot.on('callback_query', function onMessage(msg) {
  googleRetry = 0;
  adminResponse(msg);
}, (error) => {
    var time = new Date();
    console.log('CallbackQuery error')
    console.log("TIME:", time);
    console.log("CODE:", error.code);  // => 'EFATAL'
    console.log("MSG:", error.message);
    console.log("STACK:", error.stack);
    console.log("RESPONSE BODY:", error.response.body);
});

/**Stampa a video nel caso ci siano errori di polling */
bot.on('polling_error', (error) => {
  var time = new Date();
  console.log('polling error')
	console.log("TIME:", time);
  console.log("MSG:", error.message);
  if (error.code == 'ETELEGRAM' && error.message.substring(11,14) == '409'){
    console.log("ERROR: processo terminato, un'altra istanza del bot attiva");
    process.exit();
  }
  console.log("\n");
});

bot.on('uncaughtException', (error) => {
  var time = new Date();
  console.log('UncaughtException');
	console.log("TIME:", time);
	console.log("NODE_CODE:",error.code);
	console.log("MSG:",error.message);
  console.log("STACK:",error.stack);	
  console.log("RESPONSE BODY:", error.response.body);
});

function initializeData(){
  data = {
    actualRequests:{},
    requests:[],
    admins:[],
    colors: {},
    tts:{},
    sessionIds:{},
    fallbacks:{}
  }
  console.log('Actual data:');
  console.log(data);
  jsonWriter(data);
}

try {
  if (!fs.existsSync(utilityPath)){
    fs.mkdirSync(utilityPath);
  }
  if (!fs.existsSync(dataPath)) {
   initializeData();
  } else {
    const dataFile = fs.readFileSync(dataPath, 'utf8');
    try {
      data = JSON.parse(dataFile);
      console.log('Actual data:');
      console.log(data);
    } catch(error) {
      initializeData();
    }
  }
  if (!fs.existsSync(audioInputPath)){
    fs.mkdirSync(audioInputPath);
  }
  if (!fs.existsSync(audioOutputPath)){
    fs.mkdirSync(audioOutputPath);
  }
} catch(err) {
  console.error(err)
}